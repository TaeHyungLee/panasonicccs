// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyCo3YS6IDoEbMuz-_VWjufbXS-E5PkOyrg',
    authDomain: 'panasonicccs-20477.firebaseapp.com',
    databaseURL: 'https://panasonicccs-20477.firebaseio.com',
    projectId: 'panasonicccs-20477',
    storageBucket: 'panasonicccs-20477.appspot.com',
    messagingSenderId: '605136274907'
  }
};
