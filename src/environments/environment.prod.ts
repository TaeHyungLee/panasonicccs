export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyCo3YS6IDoEbMuz-_VWjufbXS-E5PkOyrg',
    authDomain: 'panasonicccs-20477.firebaseapp.com',
    databaseURL: 'https://panasonicccs-20477.firebaseio.com',
    projectId: 'panasonicccs-20477',
    storageBucket: 'panasonicccs-20477.appspot.com',
    messagingSenderId: '605136274907'
  }
};
