import {BrowserModule, DomSanitizer} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';

import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NavtoolbarModule} from './navtoolbar/navtoolbar.module';
import {NavtoolbarComponent} from './navtoolbar/navtoolbar.component';
import {CompanylistModule} from './CompanyList/companylist.module';
import {NotelistModule} from './NoteList/notelist.module';

import {FirebaseauthModule} from './firebase/firebase.module';
import {environment} from '../environments/environment';
import {RouterModule, Routes} from '@angular/router';
import {AngularFireModule} from 'angularfire2';
import {AuthGuard} from './firebase/auth.guard';
import {LoginpageModule} from './loginpage/loginpage.module';
import {LoginpageComponent} from './loginpage/loginpage.component';
import {AngularFirestoreModule} from 'angularfire2/firestore';

const appRoutes: Routes = [
  {
    path: 'login', component: LoginpageComponent
  },
  {
    path: 'main', component: NavtoolbarComponent, canActivate: [AuthGuard]
  },
  {
    path: '', redirectTo: '/main', pathMatch: 'full'
  },
  {
    path: '**', redirectTo: '/main'
  }
];

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    NavtoolbarModule,
    CompanylistModule,
    NotelistModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule.enablePersistence(),
    FirebaseauthModule,
    LoginpageModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
