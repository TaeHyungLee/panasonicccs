import {Component, Output, OnInit, EventEmitter, Input} from '@angular/core';
import {AngularFireAuth} from 'angularfire2/auth';
import {FormGroup} from '@angular/forms';

export interface Company {
  id?: string;
  title: string;
  content: string;
  user: string;
  timestamp?: string;
}

@Component({
  selector: 'app-companylist',
  templateUrl: './companylist.component.html',
  styleUrls: ['./companylist.component.css']
})
export class CompanylistComponent implements OnInit {
  @Output() onOpened: EventEmitter<Company> = new EventEmitter();
  @Output() onAdd: EventEmitter<any> = new EventEmitter();
  @Output() onDelete: EventEmitter<Company> = new EventEmitter();
  @Output() onUpdateSave: EventEmitter<Company> = new EventEmitter();

  @Input() companies: Company;
  @Input() compExIdx = -1;
  @Input() compEdMode = false;

  companyForm: FormGroup;
  currentUser = null;

  opened(idx) {
    this.compExIdx = idx;
    this.compEdMode = false;
    this.onOpened.emit(this.companies[idx]);
  }

  saveContent(company, content) {
    this.compEdMode = false;
    let title = '';
    const hasBr = content.indexOf('\n') > -1;
    if (hasBr) {
      title = content.substring(0, content.indexOf('\n'));
    } else {
      title = content;
    }

    const newCompany = JSON.parse(JSON.stringify(company));
    newCompany.title = title;
    newCompany.content = content;

    this.onUpdateSave.emit(newCompany);
  }

  delete(company) {
    this.compEdMode = false;
    this.onDelete.emit(company);
  }

  constructor(private afAuth: AngularFireAuth) {
    this.afAuth.authState.subscribe(user => {
      this.currentUser = user;
    });
  }

  add() {
    this.onAdd.emit();
  }

  ngOnInit() {
    this.companyForm = new FormGroup({});
    console.log('AGG', this.companyForm);
  }

}
