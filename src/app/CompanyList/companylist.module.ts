import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MaterialComponentsModule} from '../material-components/material-components.module';
import {CompanylistComponent} from './companylist.component';
import {StoreModule} from '../store/store.module';
import {ReactiveFormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    MaterialComponentsModule,
    StoreModule,
    ReactiveFormsModule
  ],
  exports: [
    CompanylistComponent
  ],
  declarations: [
    CompanylistComponent
  ]
})
export class CompanylistModule {
}
