import {Component, OnInit} from '@angular/core';
import {AuthService} from '../firebase/auth.service';
import {Router} from '@angular/router';
import {Note} from '../NoteList/notelist.component';
import {User} from 'firebase/app';
import {DataService} from '../store/data.service';
import {Company, CompanylistComponent} from '../CompanyList/companylist.component';
import {NotelistComponent} from '../NoteList/notelist.component';

@Component({
  selector: 'app-navtoolbar',
  templateUrl: './navtoolbar.component.html',
  styleUrls: ['./navtoolbar.component.css']
})

export class NavtoolbarComponent implements OnInit {
  currentUser: User = null;
  lastestSearchLength = -1;

  constructor(public authService: AuthService, router: Router, public dataS: DataService) {
    this.authService.afAuth.authState.subscribe(user => {
      this.currentUser = user;

      if (this.dataS.companies.length === 0) {
        this.addCompany();
      }
    });
  }

  logout(): void {
    this.dataS.currentUser = null;
    this.dataS.currentCompany = null;
    this.authService.signOut();
  }

  addCompany(): void {
    const dummy: Company = {
      title: 'New Company',
      content: 'New Company',
      user: this.currentUser.uid,
      timestamp: (new Date()).getTime() + ''
    };
    this.dataS.addCompany(dummy)
      .then(dr => {
        this.dataS.compExIdx = 0;
        this.dataS.compEdMode = false;
        dummy.id = dr.id;
        this.init(dummy);
      });
  }

  addNote(): void {
    if (!this.dataS.currentCompany) {
      return;
    }

    const dummy: Note = {
      title: 'New Company',
      content: 'New Company',
      company: this.dataS.currentCompany.id,
      timestamp: (new Date()).getTime() + ''
    };

    this.dataS.addNote(this.dataS.currentCompany, dummy)
      .then(() => {
        this.dataS.noteExIdx = 0;
        this.dataS.noteEdMode = false;
      });
  }

  init(company) {
    this.dataS.noteExIdx = -1;
    this.dataS.currentCompany = company;
    this.dataS.initNoteList(company);
  }

  deleteCompany(company) {
    this.dataS.deleteCompany(company)
      .then(() => this.dataS.compExIdx = -1);
  }

  deleteNote(note) {
    this.dataS.deleteNote(note)
      .then(() => this.dataS.noteExIdx = -1);
  }

  onSearchChange(value) {
    if (value.length < this.lastestSearchLength) {
      this.dataS.restoreNotes();

      if (value.length < 1) {
        return;
      }
    }
    this.lastestSearchLength = value.length;
    this.dataS.noteExIdx = -1;

    const filteredPreList = [];
    this.dataS.notes.forEach(note => {
      const idx = note.content.indexOf(value);
      if (idx > -1) {
        note.pre = idx;
        filteredPreList.push(
          note
        );
      }
    });

    filteredPreList.sort((a, b) => {
      return a - b;
    });

    this.dataS.fetchNotes(filteredPreList);
  }

  updatesaveComp(comp: Company): void {
    this.dataS.updateCompany(comp);
  }

  updatesaveNote(note: Note): void {
    this.dataS.updateNote(note);
  }

  ngOnInit() {
  }
}
