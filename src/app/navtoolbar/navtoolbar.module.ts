import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NavtoolbarComponent} from './navtoolbar.component';
import {MaterialComponentsModule} from '../material-components/material-components.module';
import {CompanylistModule} from '../CompanyList/companylist.module';
import {CompanylistComponent} from '../CompanyList/companylist.component';
import {NotelistComponent} from '../NoteList/notelist.component';
import {NotelistModule} from '../NoteList/notelist.module';

@NgModule({
  imports: [
    CommonModule,
    MaterialComponentsModule,
    CompanylistModule,
    NotelistModule
  ],
  declarations: [NavtoolbarComponent],
  exports: [
    NavtoolbarComponent
  ],
  providers: [CompanylistComponent, NotelistComponent]
})
export class NavtoolbarModule {
}
