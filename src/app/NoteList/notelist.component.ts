import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

export interface Note {
  id?: string;
  title: string;
  content: string;
  company: string;
  timestamp?: string;
}

@Component({
  selector: 'app-notelist',
  templateUrl: './notelist.component.html',
  styleUrls: ['./notelist.component.css']
})
export class NotelistComponent implements OnInit {
  @Input() notes = [];
  @Input() noteExIdx = -1;
  @Input() noteEdMode = false;
  @Input() hasCompany = null;

  @Output() onDelete: EventEmitter<Note> = new EventEmitter();
  @Output() onUpdateSave: EventEmitter<Note> = new EventEmitter();

  constructor() {}

  opened(idx) {
    console.log('opened:', idx);
    this.noteExIdx = idx;
    this.noteEdMode = false;
  }

  delete(note: Note): void {
    this.noteEdMode = false;
    this.onDelete.emit(note);
  }

  saveContent(note: Note, content: string): void {
    this.noteEdMode = false;
    let title = '';
    const hasBr = content.indexOf('\n') > -1;
    if (hasBr) {
      title = content.substring(0, content.indexOf('\n'));
    } else {
      title = content;
    }

    const newNote = JSON.parse(JSON.stringify(note));
    newNote.title = title;
    newNote.content = content;

    this.onUpdateSave.emit(newNote);
  }

  ngOnInit() {
  }

}
