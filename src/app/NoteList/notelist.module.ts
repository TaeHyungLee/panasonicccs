import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NotelistComponent} from './notelist.component';
import {MaterialComponentsModule} from '../material-components/material-components.module';
import {StoreModule} from '../store/store.module';

@NgModule({
  imports: [
    CommonModule,
    MaterialComponentsModule,
    StoreModule
  ],
  exports: [
    NotelistComponent
  ],
  declarations: [NotelistComponent]
})
export class NotelistModule {
}
