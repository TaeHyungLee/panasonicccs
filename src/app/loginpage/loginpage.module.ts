import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginpageComponent } from './loginpage.component';
import {RouterModule} from '@angular/router';
import {MaterialComponentsModule} from '../material-components/material-components.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    MaterialComponentsModule
  ],
  exports: [
    LoginpageComponent
  ],
  declarations: [LoginpageComponent]
})
export class LoginpageModule { }
