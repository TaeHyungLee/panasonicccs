import {Injectable} from '@angular/core';
import {AuthService} from '../firebase/auth.service';
import {Company} from '../CompanyList/companylist.component';
import {AngularFirestore} from 'angularfire2/firestore';
import {Observable} from 'rxjs/Observable';
import {User} from 'firebase/app';
import {Note} from '../NoteList/notelist.component';

@Injectable()
export class DataService {
  currentUser: User = null;
  currentCompany: Company = null;
  companies = [];
  savedCompanies = [];
  notes = [];
  noteExIdx = -1;
  compExIdx = -1;
  noteEdMode = false;
  compEdMode = false;

  clearArr(arr): void {
    arr.splice(0, arr.length);
  }

  private getCompanies(): Observable<any> {
    return this.afs
      .collection('users')
      .doc(this.currentUser.uid)
      .collection('companies', ref => ref.orderBy('timestamp', 'desc'))
      .snapshotChanges()
      .map(actions => {
        return actions.map(act => {
          const data = act.payload.doc.data();
          data.id = act.payload.doc.id;
          return data;
        });
      });
  }

  initCompanyList(): void {
    this.getCompanies().subscribe(gotCompanies => {
      console.log(gotCompanies);
      this.clearArr(this.companies);
      this.companies.push.apply(this.companies, gotCompanies);
    });
  }

  private getNotes(company: Company): Observable<any> {
    return this.afs
      .collection('users')
      .doc(this.currentUser.uid)
      .collection('companies')
      .doc(company.id)
      .collection('notes', ref => ref.orderBy('timestamp', 'desc'))
      .snapshotChanges()
      .map(actions => {
        return actions.map(act => {
          const data = act.payload.doc.data();
          data.id = act.payload.doc.id;
          return data;
        });
      });
  }

  initNoteList(company: Company): void {
    this.getNotes(company).subscribe(gotNotes => {
      this.clearArr(this.notes);
      this.notes.push.apply(this.notes, gotNotes);

      this.clearArr(this.savedCompanies);
      this.savedCompanies.push.apply(this.savedCompanies, gotNotes);
    });
  }

  addCompany(data: Company): Promise<any> {
    return this.afs.collection('users').doc(this.currentUser.uid).collection('companies').add(data)
      .then(dr => {
        this.initCompanyList();
        return Promise.resolve(dr);
      });
  }

  addNote(company: Company, data: Note): Promise<any> {

    return this.afs
      .collection('users')
      .doc(this.currentUser.uid)
      .collection('companies')
      .doc(data.company)
      .collection('notes')
      .add(data)
      .then(dr => {
        this.initNoteList(company);
        // const added: Note = {
        //   id: dr.id,
        //   title: data.title,
        //   content: data.content,
        //   company: data.company
        // };
        // this.notes.push(added);
      });
  }

  deleteCompany(company: Company): Promise<any> {
    // todo nested delete not work.
    return this.afs.collection('users')
      .doc(this.currentUser.uid)
      .collection('companies')
      .doc(company.id)
      .delete()
      .then(dr => {
        this.clearArr(this.notes);
        this.currentCompany = null;
      })
      .catch(err => {
        console.log('ERR', err);
      });
  }

  deleteNote(note: Note): Promise<any> {
    return this.afs
      .collection('users')
      .doc(this.currentUser.uid)
      .collection('companies')
      .doc(note.company)
      .collection('notes')
      .doc(note.id)
      .delete()
      .then(dr => {
        for (let i = 0; i < this.notes.length; i++) {
          if (this.notes[i].id === note.id) {
            this.notes.splice(i, 1);
            break;
          }
        }
      })
      .catch(err => {
        console.log('ERR', err);
      });
  }

  updateCompany(newCompany: Company): void {
    this.afs
      .collection('users')
      .doc(this.currentUser.uid)
      .collection('companies')
      .doc(newCompany.id)
      .update(newCompany)
      .then(dr => {

      });
  }

  updateNote(newNote: Note): void {
    this.afs
      .collection('users')
      .doc(this.currentUser.uid)
      .collection('companies')
      .doc(newNote.company)
      .collection('notes')
      .doc(newNote.id)
      .update(newNote)
      .then(dr => {

      });
  }

  restoreNotes(): void {
    this.clearArr(this.notes);
    this.notes.push.apply(this.notes, this.savedCompanies);
  }

  fetchNotes(arr): void {
    this.clearArr(this.notes);
    this.notes.push.apply(this.notes, arr);
  }

  constructor(public authS: AuthService, private afs: AngularFirestore) {
    this.authS.afAuth.authState.subscribe(user => {
      this.currentUser = user;

      this.initCompanyList();
    });
  }
}
