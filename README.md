# Ccs

This app is a note service for content collaboration.

## API

[CompanyList](doc/CompanyListKeynt.pdf)

[NoteList](doc/NoteListKeynt.pdf)


## Test

You can try using this app [here](http://panasonic-ccs.s3-website.ap-northeast-2.amazonaws.com/main).

## How to use the this app

### Authentication

![login](img/login.png)

- You can choose between Google and Github


### Add Company

![addcomp](img/newcomp.png)

- You can add your company through the float action button click.


### Edit Company Infomation

![editcomp](img/compedit.png)

- You can edit your company throught 'Edit', 'Del' button.


### Add Note To Company

![addnote](img/newnote.png)

- You can add note to your company.  (You should select a company.)


### Searching for notes.

![searchnote](img/search.png)

- You can find notes through a string search.


## Feedback

Please give feedback here.  -   leeth0610@gmail.com
